﻿namespace PickiMockRestAPI.Models
{
    public class FacebookData
    {
        public string Id { get; set; }
        public string Access_token { get; set; }
        public string expiration_date { get; set; }
    }
}