﻿namespace PickiMockRestAPI.Models
{
    public class User
    {
        public int Id { get; set; }
        public string username { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public FacebookData Facebook { get; set; }
    }
}