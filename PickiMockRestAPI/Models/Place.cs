﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PickiMockRestAPI.Models
{
    public class Place
    {
        public int Id { get; set; }

        public string Title { get; set; }

        public string Description { get; set; }

        public Location Location { get; set; }

        public double Latitude { get; set; }

        public double Longitude { get; set; }

        public string Address { get; set; }

        public string Website { get; set; }

        public string Phone { get; set; }

        public string Image { get; set; }

        public string ImageThumb { get; set; }

        public string ImageTwo { get; set; }

        public string ImageThree { get; set; }

        public string ImageFour { get; set; }

        public Category Category { get; set; }

        public int RatingCount { get; set; }

        public  int RatingTotal { get; set; }

        public int Rating { get; set; }

        public IEnumerable<Review> Reviews { get; set; }

        public Place()
        {
            Category = new Category();
        }

    }
}