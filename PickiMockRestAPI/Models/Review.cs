﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PickiMockRestAPI.Models
{
    public class Review
    {
        public string Comment { get; set; }
        public Place Place { get; set; }
        public User UserData { get; set; }
        public int Rating { get; set; }
    }
}