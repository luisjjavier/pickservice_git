﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PickiMockRestAPI.Models
{
    public class Product
    {
        public string  Title { get; set; }
        public string Description { get; set; }
        public double Price { get; set; }
    }
}