﻿using Newtonsoft.Json;
using System.IO;

namespace PickiMockRestAPI.Utils
{
    public static class JsonUtils
    {
        public static T FromJson<T>(this TextReader reader)
           => (T)(new JsonSerializer()).Deserialize(reader, typeof(T));

        public static string ToJson<T>(this T obj)
          => JsonConvert.SerializeObject(obj);
    }
}