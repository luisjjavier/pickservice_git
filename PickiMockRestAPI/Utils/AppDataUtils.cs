﻿using PickiMockRestAPI.Models;
using System.Collections.Generic;
using System.IO;
using System;

namespace PickiMockRestAPI.Utils
{
    internal class AppDataUtils
    {
        internal static List<User> LoadUsers(string path) 
            => TryLoadData<List<User>>(path);

        internal static void SaveUser(string path, List<User> users)
            => TryToSaveData<List<User>>(path,users);

        private static void TryToSaveData<T>(string path, T data)
        {
            if (path == null)
            {
                return;
            }

            string json = data.ToJson();

            File.WriteAllText(path, json);
        }

        private static T TryLoadData<T>(string path)
        {
            if (path == null)
            {
                return default(T);
            }

            T data;
            using (var file = File.OpenText(path))
            {
                data = file.FromJson<T>();
            }

            return data;
        }

    }
}