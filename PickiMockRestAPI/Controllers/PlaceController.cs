﻿using PickiMockRestAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

namespace PickiMockRestAPI.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class PlaceController : ApiController
    {
       
        [Route("api/place/category/{categoryId}")]
        public IEnumerable<Place> Get(string categoryId)
        {
            IEnumerable<Place> places = PrepareMockPlaces().Where(place => place.Category.Id.ToString() == categoryId);

            return places;
        }

        public Place Get(int id)
        {
            Place places = PrepareMockPlaces().FirstOrDefault(place => place.Id == id);

            return places;
        }

        [Route("api/place/{placeId}/Like/{userId}")]
        [HttpGet]
        public HttpResponseMessage Like(int placeId, int userId)
        {
            return Request.CreateResponse(HttpStatusCode.OK, true);
        }


        [Route("api/place/{placeId}/isStarred/{userId}")]
        [HttpGet]
        public HttpResponseMessage isStarred(int placeId, int userId)
        {
            return Request.CreateResponse(HttpStatusCode.OK, false);
        }

        [Route("api/place/{placeId}/IsLike/{userId}")]
        [HttpGet]
        public HttpResponseMessage IsLike(int placeId, int userId)
        {
            return Request.CreateResponse(HttpStatusCode.OK, true);
        }

        [Route("api/place/search/{q}")]
        [HttpGet]
        [EnableCors(origins: "*", headers: "*", methods: "*")]
        public IEnumerable<Place> Find(string q)
        {
            IEnumerable<Place> places = PrepareMockPlaces().Where(place => place.Title.ToUpperInvariant().Contains(q.ToUpperInvariant()));

            return places;
        }



        [Route("api/place/favorites/{userId}")]
        [HttpGet]
        [EnableCors(origins: "*", headers: "*", methods: "*")]
        public IEnumerable<Place> Favorites(int userId)
        {
            IEnumerable<Place> places = PrepareMockPlaces();

            return places;
        }

        private IEnumerable<Place> PrepareMockPlaces()
        {
            var places = new List<Place>();

            List<Review> reviews = PrepareMockReviews();
            places.Add(new Place()
            {
                Id = 1,
                Title = "Sherezade",
                Address = "Avenida Roberto Pastoriza, Santo Domingo, República Dominicana",
                Description = "Comida mediterranea. Excelente selección de vinos, y cocina de finos ingredientes frescos de lo mejor del mundo. Celebraciones de eventos. Restaurante galardonado entre los mejores del Caribe.",
                Image = "https://scontent.fsdq1-2.fna.fbcdn.net/v/t1.0-9/1901243_10152209093979244_295296952_n.jpg?oh=68635001f8396f5b32a3eb3e083d1816&oe=58BE44EE",
                ImageFour = "https://scontent.fsdq1-2.fna.fbcdn.net/v/t1.0-9/14724627_10154595096419244_8812405419222466758_n.jpg?oh=1518246174a5826dafbef198de905b6f&oe=58B1C3DF",
                ImageThree = "https://scontent.fsdq1-2.fna.fbcdn.net/v/t1.0-9/14572363_10154584180599244_3108515609666554389_n.jpg?oh=89bb2b4536920e22b7f0ea5bd9b13f78&oe=58BE1CFF",
                ImageTwo = "http://www.scherezaderestaurant.com/content/967157/menu_completo.jpg",
                ImageThumb = "https://scontent.fsdq1-2.fna.fbcdn.net/v/t1.0-9/1901243_10152209093979244_295296952_n.jpg?oh=68635001f8396f5b32a3eb3e083d1816&oe=58BE44EE",
                Latitude = 18.4722761,
                Longitude = -69.9249814,
                Phone = "809-332-5200",
                Rating = 4,
                RatingCount = 4,
                RatingTotal = 4,
                Category = new Category()
                {
                    Id = 1,
                    Description = "Restaurantes mas exclusivos",
                    Icon = "ion-fork",
                    Title = "Restaurantes",
                    Image = "http://cdn.theculturetrip.com/wp-content/uploads/2016/02/restaurant-939435_960_720.jpg",
                },
                Reviews = reviews

            });
            places.Add(new Place()
            {
                Id = 2,
                Title = "Lu•Ga",
                Address = "Av Bolivar 277, Santo Domingo, República Dominicana",
                Description = "We use only the best ingredients that keep customers coming back again and again",
                Image = "https://scontent.fsdq1-2.fna.fbcdn.net/v/t1.0-9/1901243_10152209093979244_295296952_n.jpg?oh=68635001f8396f5b32a3eb3e083d1816&oe=58BE44EE",
                ImageFour = "https://media-cdn.tripadvisor.com/media/photo-s/0c/ee/b9/58/dsc-3148-largejpg.jpg",
                ImageThree = "https://bocao.com.do/uploads/img03-01-2015-596169084.jpg",
                ImageTwo = "https://bocao.com.do/uploads/img03-01-2015-874354521.jpg",
                ImageThumb = "http://nebula.wsimg.com/4291b1adb54597ab5d1a3f1cc7042d30?AccessKeyId=57AFD88B7DF514FD141A&disposition=0&alloworigin=1",
                Latitude = 18.4617979,
                Longitude = -69.9335522,
                Phone = "+1 829-998-9090",
                Rating = 5,
                RatingCount = 5,
                RatingTotal = 5,
                Website = "http://www.lugadr.com/",
                Category = new Category()
                {
                    Id = 1,
                    Description = "Restaurantes mas exclusivos",
                    Icon = "ion-fork",
                    Title = "Restaurantes",
                    Image = "http://cdn.theculturetrip.com/wp-content/uploads/2016/02/restaurant-939435_960_720.jpg",
                },
               

            });

            places.Add(new Place()
            {
                Id = 3,
                Title = "Loop Friends Burgers",
                Address = " Av Rómulo Betancourt 57, Santo Domingo 10114, República Dominicana",
                Description = "Para los que les gusta probar algo diferente👌",
                Image = "https://scontent.fsdq1-2.fna.fbcdn.net/v/t1.0-9/11203004_467668306715858_3362911139076766258_n.jpg?oh=bcc9a5ae186333c75e9be1eda1534172&oe=58FC72E0",
                ImageFour = "https://bocao.com.do/uploads/img26-02-2016-603497563.jpg",
                ImageThree = "https://bocao.com.do/uploads/img26-02-2016-480341449.jpg",
                ImageTwo = "https://bocao.com.do/uploads/img26-02-2016-1130257135.jpg",
                ImageThumb = "https://scontent.fsdq1-2.fna.fbcdn.net/v/t1.0-9/11203004_467668306715858_3362911139076766258_n.jpg?oh=bcc9a5ae186333c75e9be1eda1534172&oe=58FC72E0",
                Latitude = 18.4484059,
                Longitude = -69.9599211,
                Phone = "+1 809-937-3121",
                Rating = 5,
                RatingCount = 5,
                RatingTotal = 5,
                Website = "https://www.facebook.com/Loopburgers/",
                Category = new Category()
                {
                    Id = 1,
                    Description = "Restaurantes mas exclusivos",
                    Icon = "ion-fork",
                    Title = "Restaurantes",
                    Image = "http://cdn.theculturetrip.com/wp-content/uploads/2016/02/restaurant-939435_960_720.jpg",
                },
                Reviews = reviews
            });

            places.Add(new Place()
            {
                Id = 4,
                Title = "Agora Mall",
                Address = " Av. John F. Kennedy Santo Domingo",
                Description = "Ofrece una experiencia memorable con una variada mezcla de tiendas con exitosas marcas nacionales e internacionales. El Jardín tiene la mayor variedad de restaurantes y entretenimiento en toda la República Dominicana",
                Image = "https://scontent.fsdq1-2.fna.fbcdn.net/v/t1.0-9/10647245_897879313576202_6515154825041121436_n.png?oh=de19b39441e9d912f7d9848543efea11&oe=58BA91E1",
                ImageFour = "http://www.agora.com.do/home/images/calendario.png",
                ImageThree = "http://www.agora.com.do/home/images/mercadito-s.jpg",
                ImageTwo = "http://www.agora.com.do/home/images/novedades/agora-mall.jpg",
                ImageThumb = "https://scontent.fsdq1-2.fna.fbcdn.net/v/t1.0-9/10647245_897879313576202_6515154825041121436_n.png?oh=de19b39441e9d912f7d9848543efea11&oe=58BA91E1",
                Latitude = 18.483469,
                Longitude = -69.9414483,
                Phone = "+1 809-219-9754",
                Rating = 5,
                RatingCount = 5,
                RatingTotal = 5,
                Website = "http://www.agora.com.do/",
                Category = new Category()
                {
                    Id = 2,
                    Description = "Tiendas exclusivas",
                    Icon = "ion-bag",
                    Title = "Tiendas",
                    Image = "http://i.dailymail.co.uk/i/pix/2014/05/03/article-2619548-1BA952D9000005DC-213_634x339.jpg",
                },
                Reviews = reviews
            });

            places.Add(new Place()
            {
                Id = 5,
                Title = "Mega Centro",
                Address = "Av San Vicente de Paúl, Santo Domingo Este 11519, República Dominicana",
                Description = "Megacentro es la primera plaza comercial de la Zona Oriental, donde podrás encontrar más de 200 tiendas y 2,800 parqueos, un Food Court para todos los gustos y un mundo de entretenimiento.",
                Image = "http://megacentro.com.do/img/logo.png",
                ImageFour = "http://megacentro.com.do/img/actividades/8%20FERIA%20DE%20VERANO%20MEGACENTRO/3.jpg",
                ImageThree = "http://megacentro.com.do//img/actividades/6%20MEGA%20COOKING%20SHOW%202/PORTADA.jpg",
                ImageTwo = "http://megacentro.com.do//img/actividades/6%20MEGA%20COOKING%20SHOW%202/PORTADA.jpg",
                ImageThumb = "http://megacentro.com.do/img/logo.png",
                Latitude = 18.5071625,
                Longitude = -69.8565084,
                Phone = "809 236 7660",
                Rating = 5,
                RatingCount = 5,
                RatingTotal = 5,
                Website = "http://www.megacentro.com.do/",
                Category = new Category()
                {
                    Id = 2,
                    Description = "Tiendas exclusivas",
                    Icon = "ion-bag",
                    Title = "Tiendas",
                    Image = "http://i.dailymail.co.uk/i/pix/2014/05/03/article-2619548-1BA952D9000005DC-213_634x339.jpg",
                },
                Reviews = reviews
            });


            return places;
        }


        

        private List<Review> PrepareMockReviews()
        {
            var reviews = new List<Review>();

            reviews.Add(new Review
            {
                Comment = "Comentario 1",
                Rating = 4,
                UserData = new Models.User
                {
                    username = "Juancho",
                    Id = 2,
                    Name = "Juan luis",
                }
            });

            reviews.Add(new Review
            {
                Comment = "Comentario 4",
                Rating = 2,
                UserData = new Models.User
                {
                    username = "Juancho3",
                    Id = 4,
                    Name = "Juan guzman",
                }
            });

            reviews.Add(new Review
            {
                Comment = "Comentario 3",
                Rating = 5,
                UserData = new Models.User
                {
                    username = "juancho2",
                    Id = 6,
                    Name = "Juan pedro",
                }
            });
            return reviews;
        }
    }
}
