﻿using PickiMockRestAPI.Models;
using PickiMockRestAPI.Utils;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Hosting;
using System.Web.Http;
using System.Web.Http.Cors;

namespace PickiMockRestAPI.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class UserController : ApiController
    {
        private const string UsersAppFileKey = "userFileVirtualPath";
        private readonly string usersFileVirtualPath;

        public UserController()
        {
            usersFileVirtualPath = HostingEnvironment.MapPath(GetConfiguration(UsersAppFileKey));
        }

        private string GetConfiguration(string key)
            => ConfigurationManager.AppSettings[key] ?? string.Empty;

        [Route("api/user/authenticate")]
        [HttpPost]
        public IHttpActionResult Authenticate(User account)
        {
            List<User> users = AppDataUtils.LoadUsers(usersFileVirtualPath);

            User user = users.FirstOrDefault(u => u.Email.ToUpperInvariant().Equals(account.Email.ToUpperInvariant())
                                               && u.Password.ToUpperInvariant().Equals(account.Password.ToUpperInvariant()));
            if (user == null)
            {
                return BadRequest();
            }

            return Ok(user);
        }

        [Route("api/user")]
        [HttpPost]
        public IHttpActionResult Register(User newUser)
        {
            List<User> users = AppDataUtils.LoadUsers(usersFileVirtualPath);

            User foundUser = users.FirstOrDefault(u => u.Email.ToUpperInvariant().Equals(newUser.Email.ToUpperInvariant()));

            if (foundUser != null)
            {
                return Conflict();
            }

            newUser.Id = users.Count + 1;

            users.Add(newUser);

            AppDataUtils.SaveUser(usersFileVirtualPath, users);

            return Ok(newUser);
        }

        [Route("api/user")]
        [HttpPut]
        public IHttpActionResult UpdateDate(User newUser)
        {
            List<User> users = AppDataUtils.LoadUsers(usersFileVirtualPath);

            User foundUser = users.FirstOrDefault(u => u.Email.ToUpperInvariant().Equals(newUser.Email.ToUpperInvariant()));

            if (foundUser != null)
            {
                return Ok(foundUser);
            }

            newUser.Id = users.Count + 1;

            users.Add(newUser);

            AppDataUtils.SaveUser(usersFileVirtualPath, users);

            return Ok(newUser);
        }

        [Route("api/user/email/{email}")]
        [HttpGet]
        public IHttpActionResult FindByEmail(string email)
        {
            List<User> users = AppDataUtils.LoadUsers(usersFileVirtualPath);

            User foundUser = users.FirstOrDefault(u => u.Email.ToUpperInvariant().Equals(email.ToUpperInvariant()));

            if (foundUser == null)
            {
              var user  = new User() { Id = 0 };
              return Ok(user);
            }
            return Ok(foundUser);
        }

    }
}
