﻿using PickiMockRestAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

namespace PickiMockRestAPI.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class ProductController : ApiController
    {
        [HttpGet]
        [Route("api/product/place/{placeId}")]
        public IEnumerable<Product> GetPlacesProducts(int placeId)
        {
            return PrepareProductsMock();
        }

        private IEnumerable<Product> PrepareProductsMock()
        {
            var products = new List<Product>();

            products.Add(new Product
            {
                Price = 2.32,
                Description = "Los mejores tennis jordan",
                Title = "Tennis"
            });

            products.Add(new Product
            {
                Price = 2,
                Description = "Los mejores raquetas jordan",
                Title = "raquetas"
            });

            products.Add(new Product
            {
                Price = 100,
                Description = "Los mejores T-shirt",
                Title = " T-shirt"
            });

            products.Add(new Product
            {
                Price = 4,
                Description = "Los mejores collares",
                Title = "collares"
            });


            return products;
        }


    }
}
