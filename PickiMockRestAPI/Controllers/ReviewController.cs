﻿using PickiMockRestAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

namespace PickiMockRestAPI.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class ReviewController : ApiController
    {
        [Route("api/review/place/{id}")]
        [HttpGet]
        public IEnumerable<Review> AllReviewsFromPlace (int id)
        {
            return PrepareMockReviews();
        }

        [Route("api/review")]
        [HttpPost]
        public Review Create(Review review)
        {
            return review;
        }

        private List<Review> PrepareMockReviews()
        {
            var reviews = new List<Review>();

            reviews.Add(new Review
            {
                Comment = "Comentario 1",
                Rating = 4,
                UserData = new Models.User
                {
                    username = "Juancho",
                    Id = 2,
                    Name = "Juan luis",
                }
            });

            reviews.Add(new Review
            {
                Comment = "Comentario 4",
                Rating = 2,
                UserData = new Models.User
                {
                    username = "Juancho3",
                    Id = 4,
                    Name = "Juan guzman",
                }
            });

            reviews.Add(new Review
            {
                Comment = "Comentario 3",
                Rating = 5,
                UserData = new Models.User
                {
                    username = "juancho2",
                    Id = 6,
                    Name = "Juan pedro",
                }
            });
            return reviews;
        }
    }
}
