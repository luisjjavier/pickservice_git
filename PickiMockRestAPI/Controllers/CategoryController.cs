﻿using PickiMockRestAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

namespace PickiMockRestAPI.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class CategoryController : ApiController
    {
        public IEnumerable <Category> Get()
        {
            IEnumerable<Category> categories = PrepareCategories();
            
            return categories;
        }

        private IEnumerable<Category> PrepareCategories()
        {
            var categories = new List<Category>();

            categories.Add(new Category()
            {
                Id = 1,
                Description = "Restaurantes mas exclusivos",
                Icon  = "ion-fork",
                Title = "Restaurantes",
                Image = "http://cdn.theculturetrip.com/wp-content/uploads/2016/02/restaurant-939435_960_720.jpg", 
            });

            categories.Add(new Category()
            {
                Id = 2,
                Description = "Tiendas exclusivas",
                Icon = "ion-bag",
                Title = "Tiendas",
                Image = "http://i.dailymail.co.uk/i/pix/2014/05/03/article-2619548-1BA952D9000005DC-213_634x339.jpg",
            });

            categories.Add(new Category()
            {
                Id = 3,
                Description = "Bares mas exclusivos",
                Icon = "ion-beer",
                Title = "Bares",
                Image = "http://uploads.thompsonhotels.com/2014/11/LP_Bar-1200x667.jpg",
            });


            return categories;
        }
    }
}
